package servicelego

import (
	"gitlab.com/project_falcon/kubeless/klib/job"
	"gitlab.com/project_falcon/kubeless/klib/toolslego"
	"gitlab.com/project_falcon/kubeless/lib/payload"
	apiV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	service = "svclego"
)

func getPortsList(jobDef *job.BuildConfig) []apiV1.ServicePort {
	portList := []apiV1.ServicePort{}

	for _, container := range jobDef.Run {
		if len(container.Ports) > 0 {
			for name, port := range container.Ports {
				p := apiV1.ServicePort{
					Name:     name,
					Port:     port,
					Protocol: apiV1.ProtocolTCP,
				}

				portList = append(portList, p)
			}
		}
	}

	return portList
}

func getServiceSelector(deployName string) map[string]string {
	selector := make(map[string]string)
	selector["app"] = deployName

	return selector
}

func GetServiceDef(apiEvent *payload.APIData, box *payload.Box, jobDef *job.BuildConfig) *apiV1.Service {

	deployName := toolslego.GetBaseName(payload.DK8sCreateDeploy, box.Project)

	svc := apiV1.Service{
		ObjectMeta: metaV1.ObjectMeta{
			Name:   deployName,
			Labels: toolslego.GetBoxLabels(*box, deployName),
		},

		Spec: apiV1.ServiceSpec{
			Type:     apiV1.ServiceTypeClusterIP,
			Selector: getServiceSelector(deployName),
			Ports:    getPortsList(jobDef),
		},
	}

	return &svc
}

// app=kbox-xcaf app=xcaf
